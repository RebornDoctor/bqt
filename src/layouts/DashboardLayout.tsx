import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import DevicesIcon from '@mui/icons-material/Devices';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { NavLink, Outlet } from 'react-router-dom';
import DashboardIcon from '@mui/icons-material/Dashboard';
import ListAltIcon from '@mui/icons-material/ListAlt';

const drawerWidth = 240;

export default function DashboardLayout() {
    const [mobileOpen, setMobileOpen] = React.useState(false);

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const navLinkStyle = {
        textDecoration: "none"
    }

    const listItemTextStyle = {
        color: 'black'
    }

    const drawer = (
        <div>
            <Toolbar />
            <Divider />
            <List>
                <NavLink to="/" style={navLinkStyle}>
                    {({ isActive }) => (
                        <ListItem key={'Dashboard'} disablePadding style={{ backgroundColor: isActive ? "#8674742e" : "" }}>
                            <ListItemButton >
                                <ListItemIcon>
                                    <DashboardIcon style={{ color: 'rgba(0, 0, 0, 0.54)' }} />
                                </ListItemIcon>
                                <ListItemText primary={'Dashboard'} style={listItemTextStyle} />
                            </ListItemButton>
                        </ListItem>
                    )}
                </NavLink>
                <NavLink to="/devices" style={navLinkStyle}>
                    {({ isActive }) => (
                        <ListItem key={'Devices'} disablePadding style={{ backgroundColor: isActive ? "#8674742e" : "" }}>
                            <ListItemButton>
                                <ListItemIcon>
                                    <DevicesIcon style={{ color: 'rgba(0, 0, 0, 0.54)' }} />
                                </ListItemIcon>
                                <ListItemText primary={'Devices'} style={listItemTextStyle} />
                            </ListItemButton>
                        </ListItem>
                    )}
                </NavLink>
                <NavLink to="/graph" style={navLinkStyle}>
                    {({ isActive }) => (
                        <ListItem key={'Graph'} disablePadding style={{ backgroundColor: isActive ? "#8674742e" : "" }}>
                            <ListItemButton>
                                <ListItemIcon>
                                    <AccountTreeIcon />
                                </ListItemIcon>
                                <ListItemText primary={'Graph'} style={listItemTextStyle} />
                            </ListItemButton>
                        </ListItem>
                    )}
                </NavLink>
                <NavLink to="/toDo" style={navLinkStyle}>
                    {({ isActive }) => (
                        <ListItem key={'ToDoList'} disablePadding style={{ backgroundColor: isActive ? "#8674742e" : "" }}>
                            <ListItemButton >
                                <ListItemIcon>
                                    <ListAltIcon style={{ color: 'rgba(0, 0, 0, 0.54)' }} />
                                </ListItemIcon>
                                <ListItemText primary={'To do list'} style={listItemTextStyle} />
                            </ListItemButton>
                        </ListItem>
                    )}
                </NavLink>
            </List>
        </div >
    );

    const container = window.document.body;

    return (
        <Box sx={{ display: 'flex' }}>
            <CssBaseline />
            <AppBar
                position="fixed"
                sx={{
                    width: { sm: `calc(100% - ${drawerWidth}px)` },
                    ml: { sm: `${drawerWidth}px` },
                }}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        sx={{ mr: 2, display: { sm: 'none' } }}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" noWrap component="div">
                        BQT Dashboard
                    </Typography>
                </Toolbar>
            </AppBar>
            <Box
                component="nav"
                sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
                aria-label="mailbox folders"
            >

                <Drawer
                    container={container}
                    variant="temporary"
                    open={mobileOpen}
                    onClose={handleDrawerToggle}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                    sx={{
                        display: { xs: 'block', sm: 'none' },
                        '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                    }}
                >
                    {drawer}
                </Drawer>
                <Drawer
                    variant="permanent"
                    sx={{
                        display: { xs: 'none', sm: 'block' },
                        '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                    }}
                    open
                >
                    {drawer}
                </Drawer>
            </Box>
            <Box
                component="main"
                sx={{ flexGrow: 1, p: 3, width: { sm: `calc(100% - ${drawerWidth}px)` } }}
            >
                <Toolbar />
                <Outlet />
            </Box>
        </Box>
    );
}
