import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { useSelector } from "react-redux"
import { getCurrentDevices } from '../redux/devices';
import { Idevice } from '../types';


export default function ToDoList() {
    const currentDevices: Array<Idevice> = useSelector(getCurrentDevices);
    return (
        <div>
            <h3>Current Devices</h3>
            <TableContainer component={Paper}>
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Index</TableCell>
                            <TableCell align="right">Device Type</TableCell>
                            <TableCell align="right">Number of Devices</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {currentDevices.map((row: Idevice, index: number) => (
                            <TableRow
                                key={index}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {index + 1}
                                </TableCell>
                                <TableCell align="right">{row.type}</TableCell>
                                <TableCell align="right">{row.num}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
}