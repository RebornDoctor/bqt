import React from 'react'
import { yupResolver } from '@hookform/resolvers/yup';
import { Grid, Paper, Avatar, TextField, Button, Typography, Alert, Snackbar } from '@mui/material';
import PersonAddIcon from '@mui/icons-material/PersonAdd';
import { useForm, SubmitHandler } from 'react-hook-form';
import { NavLink } from 'react-router-dom';
import signupValidationSchema from "../validations/SignupValidation"
import "./Signup.scss"
import { registerUser } from '../utils';

interface IsingupForm {
    fullname: string
    username: string;
    password: string;
    email: string
}


function Signup(): JSX.Element {
    const { register, handleSubmit, formState: { errors }, setError } = useForm<IsingupForm>({ mode: "onTouched", resolver: yupResolver(signupValidationSchema()) });
    const [open, setOpen] = React.useState(false);

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const onSubmit: SubmitHandler<IsingupForm> = (data) => {
        const isRegistered = registerUser(data);
        if (isRegistered)
            handleClick();
        else {
            setError('username', { message: "username already exist!" })
        }
    }



    /** JSS COMPONENT STYLES */
    const paperStyle = {
        padding: 20,
        minHeight: '70vh',
        width: 280,
    }
    const singupPageContainer = {
        display: "flex",
        width: "100%",
        height: "100vh",
        alignItems: "center",
        justifyContent: "center",
    }
    const avatarStyle = {
        backgroundColor: '#1bbd7e'
    }
    const textFieldStyle = {
        marginTop: "10px",
        marginBottom: "10px",
    }
    const btnstyle = { margin: '8px 0' }

    return (
        <>
            <Snackbar open={open} autoHideDuration={3000} onClose={handleClose} >
                <Alert severity="success" sx={{ width: '100%' }} onClose={handleClose} >
                    Account Created Succssfully!
                </Alert>
            </Snackbar>
            <Grid style={singupPageContainer}>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <Paper className='signupContainer' elevation={10} style={paperStyle}>
                        <Grid container direction="column"
                            alignItems="center"
                            justifyContent="center">
                            <Avatar style={avatarStyle}><PersonAddIcon /></Avatar>
                            <h2>Sign up</h2>
                        </Grid>
                        <TextField label='Fullname*' placeholder='Enter fullname' style={textFieldStyle} fullWidth {...register("fullname")} />
                        <small style={{ color: "red" }}>{errors.fullname?.message}</small>
                        <TextField label='Email*' placeholder='Enter email' style={textFieldStyle} fullWidth {...register("email")} />
                        <small style={{ color: "red" }}>{errors.email?.message}</small>
                        <TextField label='Username*' placeholder='Enter username' style={textFieldStyle} fullWidth {...register("username")} />
                        <small style={{ color: "red" }}>{errors.username?.message}</small>
                        <TextField label='Password*' placeholder='Enter Password' style={textFieldStyle} type={"password"} fullWidth {...register("password")} />
                        <small style={{ color: "red" }}>{errors.password?.message}</small>
                        <Button type='submit' color='primary' variant="contained" style={btnstyle} fullWidth>Sign up</Button>
                        <Typography mb={5}> Do you have an account ?
                            <NavLink to="/login" style={{ marginLeft: "3px", color: "blue" }} >
                                Sign in
                            </NavLink>
                        </Typography>
                    </Paper>
                </form>
            </Grid>
        </>
    )
}

export default Signup