import { Select, MenuItem, InputLabel, FormControl, Grid, TextField, Button, Alert, Snackbar, SelectChangeEvent } from '@mui/material';
import React, { useEffect, useState } from 'react'
import Popup from '../components/PopupDialog';
import { useSelector, useDispatch } from 'react-redux';
import { allDevicesTypes } from '../redux/devices';
import { yupResolver } from '@hookform/resolvers/yup';
import { SubmitHandler, useForm } from 'react-hook-form';
import DevicesValidations from "../validations/DevicesValidations"
import { setCurrentDevices } from "../redux/devices"
import { Idevice } from '../types';

function Devices(): JSX.Element {
    const [numOfDevices, setnumOfDevices] = useState(0);
    const devicesTypes = useSelector(allDevicesTypes);
    const [filteredDeviceTypes, setfilteredDeviceTypes] = useState(devicesTypes);
    const dispatch = useDispatch();
    const deviceTypeFormInputs = [];
    const DevicesValidationSchema = DevicesValidations(numOfDevices);
    const { register, handleSubmit } = useForm<any>({ mode: "onTouched", resolver: yupResolver(DevicesValidationSchema) });
    const [open, setOpen] = React.useState(false);

    const onDeviceChange = (e: SelectChangeEvent<unknown>) => {
        const newArr = filteredDeviceTypes.map((el: any) => {
            if (el.type === e.target.value) {
                return { ...el, disabled: true }
            }
            return el
        });
        setfilteredDeviceTypes(newArr)
    }

    useEffect(() => {
        const data: { type: string; disabled: boolean; }[] = [];
        devicesTypes.map((type: string) => {
            data.push({ type: type, disabled: false })
        });
        setfilteredDeviceTypes(data);
    }, [devicesTypes])


    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    }

    const onSubmit: SubmitHandler<any> = (data) => {
        const finalData: Array<Idevice> = [];

        for (let i = 0; i < numOfDevices; i++) {
            const typeItem = data[`device-type-${i + 1}`];
            const numItem = data[`devices-numbers-${i + 1}`];
            finalData.push({ type: typeItem, num: numItem });
        }
        dispatch(setCurrentDevices(finalData))
        handleClick();
    }



    for (let i = 0; i < numOfDevices; i++) {
        deviceTypeFormInputs.push(

            <div key={i}>
                <h5>Device Category #{i + 1}</h5>
                <Grid container>
                    <Grid item md={6} >
                        <div style={{ marginBottom: "15px" }}>
                            <FormControl style={{ minWidth: "200px", width: "90%", marginRight: "10px" }}>
                                <InputLabel id={`demo-simple-select-helper-label-${i}`}>Device Type</InputLabel>
                                <Select
                                    labelId={`demo-simple-select-helper-label-${i}`}
                                    id={`demo-simple-select-${i}`}
                                    label={'choose device type'}
                                    placeholder='Select Device'
                                    {...register(`device-type-${i + 1}`)}
                                    onChange={onDeviceChange}
                                    required
                                >
                                    {
                                        filteredDeviceTypes.map((type: { type: string; disabled: boolean; }) => (
                                            <MenuItem disabled={type.disabled} key={type.type} value={type.type}>{type.type}</MenuItem>
                                        ))
                                    }
                                </Select>
                            </FormControl>
                        </div>
                    </Grid>

                    <Grid item md={6}>
                        <div style={{ marginBottom: "15px" }}>
                            <FormControl fullWidth style={{ minWidth: "200px", width: "90%" }}>
                                <TextField
                                    id={`outlined-number-${i}`}
                                    {...register(`devices-numbers-${i + 1}`)}
                                    label="Number of devices"
                                    type="number"
                                    defaultValue={1}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                                <small style={{ color: "red" }}>{ }</small>
                            </FormControl>
                        </div>
                    </Grid>
                </Grid>
            </div>
        )

    }


    return (
        <>
            <Snackbar open={open} autoHideDuration={3000} onClose={handleClose} >
                <Alert severity="success" sx={{ width: '100%' }} onClose={handleClose} >
                    Devices Added Succussfully!
                </Alert>
            </Snackbar>
            <h3>Devices Informations</h3>
            <div>
                <Popup setnumOfDevices={setnumOfDevices} />
            </div>
            <div>
                <form onSubmit={handleSubmit(onSubmit)}>
                    {deviceTypeFormInputs}
                    <Button color='primary' variant="contained" style={{ marginTop: "30px", marginRight: "30px" }}>Add Device Type</Button>
                    <Button type='submit' color='primary' variant="contained" style={{ marginTop: "30px" }}>Save</Button>
                </form>
            </div>
        </>
    )
}

export default Devices