import { useSelector } from 'react-redux';
import GraphComponent from "../components/GraphComponent"
import { getCurrentDevices } from "../redux/devices";
import { graphEdgesFactory } from '../utils';

function Graph(): JSX.Element {
    const currentDevices = useSelector(getCurrentDevices);
    const graph = graphEdgesFactory(currentDevices);

    return (<>
        <h3>Devices Network Graph</h3>
        <div><GraphComponent nodes={graph.nodes} edges={graph.edges} /></div>
    </>
    )
}

export default Graph