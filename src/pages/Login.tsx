import { Avatar, Button, Checkbox, FormControlLabel, Grid, Paper, TextField, Typography } from '@mui/material'
import LockIcon from '@mui/icons-material/Lock';
import React from 'react'
import "./Login.scss";
import { NavLink } from 'react-router-dom';
import { SubmitHandler, useForm } from 'react-hook-form';
import loginValidationSchema from "../validations/LoginValidations"
import { yupResolver } from '@hookform/resolvers/yup';
import { fetchUser } from '../utils';
import { useNavigate, useLocation } from "react-router-dom"
import { Iuser } from '../types';

interface IloginProps {
    setIsAuth: React.Dispatch<React.SetStateAction<boolean>>
}

interface IloginForm {
    username: string;
    password: string;
}

function Login(props: IloginProps): JSX.Element {
    const { setIsAuth } = props;
    const { register, handleSubmit, formState: { errors }, setError } = useForm<IloginForm>({ mode: "onTouched", resolver: yupResolver(loginValidationSchema()) });
    const navigate = useNavigate();
    const location = useLocation();
    const from = location.state?.from?.pathname || "/"

    const onSubmit: SubmitHandler<IloginForm> = (data) => {
        const { username, password } = data;
        const fetchedUser: Iuser | null = fetchUser(username);
        if (fetchedUser !== null) {
            if (fetchedUser.password === password) {
                setIsAuth(true);
                navigate(from, { replace: true });
            }
            else {
                setError('password', { message: 'Incorrect password' })
            }
        } else {
            setError('username', { message: 'Incorrect username' })
        }
    }

    /** JSS COMPONENT STYLES */
    const paperStyle = {
        padding: 20,
        height: '70vh',
        width: 280,
    }
    const loginPageContainer = {
        display: "flex",
        width: "100%",
        height: "100vh",
        alignItems: "center",
        justifyContent: "center",
    }
    const avatarStyle = {
        backgroundColor: '#1bbd7e'
    }
    const textFieldStyle = {
        marginTop: "10px",
        marginBottom: "10px",
    }
    const btnstyle = { margin: '8px 0' }

    return (
        <>
            <Grid style={loginPageContainer}>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <Paper className='loginContainer' elevation={10} style={paperStyle}>
                        <Grid container direction="column"
                            alignItems="center"
                            justifyContent="center">
                            <Avatar style={avatarStyle}><LockIcon /></Avatar>
                            <h2>Sign In</h2>
                        </Grid>
                        <TextField label='Username*' placeholder='Enter username' style={textFieldStyle} fullWidth {...register("username")} />
                        <small style={{ color: "red" }}>{errors.username?.message}</small>
                        <TextField label='Password*' placeholder='Enter Password' style={textFieldStyle} type={"password"} fullWidth {...register("password")} />
                        <small style={{ color: "red" }}>{errors.password?.message}</small><br></br>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    name="remember"
                                    color="primary"
                                />
                            }
                            label="Remember me"
                        />
                        <Button type='submit' color='primary' variant="contained" style={btnstyle} fullWidth>Sign in</Button>
                        <Typography >
                            <NavLink to="#" style={{ color: "blue" }} >
                                Forgot Password
                            </NavLink>
                        </Typography>
                        <Typography > Do you have an account ?
                            <NavLink to="/signup" style={{ marginLeft: "3px", color: "blue" }} >
                                Sign up
                            </NavLink>
                        </Typography>
                    </Paper>
                </form>
            </Grid>
        </>
    )
}

export default Login
