import { createSlice } from "@reduxjs/toolkit";
import { Idevice } from "../types";

export interface IdeviceState {
    types: Array<string>
    currentDevices: Array<null | Idevice>
}

const initialState: IdeviceState = {
    types: ['Router', 'Laptop', 'Server', 'Switch'],
    currentDevices: [],
};
export const devicesSlice = createSlice({
    name: "devices",
    initialState,
    reducers: {
        setCurrentDevices: (state, action) => {
            state.currentDevices = action.payload;
        },
        addDeviceType: (state, action) => {
            state.types.push(action.payload);
        }
    }
})

export const allDevicesTypes = (state: any) => state.device.types
export const getCurrentDevices = (state: any) => state.device.currentDevices

export const { setCurrentDevices, addDeviceType } = devicesSlice.actions;
export default devicesSlice.reducer;