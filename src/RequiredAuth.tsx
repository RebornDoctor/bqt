import React from 'react'
import { Navigate, useLocation } from 'react-router-dom'
import DashboardLayout from './layouts/DashboardLayout';

interface IrequiredAuth {
    isAuth: boolean
}

function RequiredAuth({ isAuth }: IrequiredAuth): JSX.Element {
    const location = useLocation();
    return isAuth ? <DashboardLayout /> : <Navigate to="/login" state={{ from: location }} replace />
}

export default RequiredAuth