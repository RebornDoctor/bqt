import React, { useState } from 'react'
import { Route, Routes } from 'react-router-dom';
import Login from './pages/Login';
import RequiredAuth from './RequiredAuth';
import "./App.scss";
import Signup from './pages/Signup';
import Devices from './pages/Devices';
import ToDoList from './pages/ToDoList';
import Graph from './pages/Graph';

function App(): JSX.Element {
  const [isAuth, setIsAuth] = useState(false);

  return (
    <>
      <Routes>
        <Route path='/'>
          {/*Public Routes */}
          <Route path='login' element={<Login setIsAuth={setIsAuth} />} />
          <Route path='signup' element={<Signup />} />

          {/*Protected Routes */}
          <Route element={<RequiredAuth isAuth={isAuth} />}>
            <Route index element={"Dashboard"} />
            <Route path='/devices' element={<Devices />} />
            <Route path='/graph' element={<Graph />} />
            <Route path='/toDo' element={<ToDoList />} />
          </Route>

          {/*Catches All Routes */}
          <Route path="*" element={<h4>NOT FOUND</h4>} />
        </Route>
      </Routes>
    </>
  )
}

export default App