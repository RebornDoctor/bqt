import * as yup from 'yup';

function SignupValidation() {

    const schema = yup.object().shape({
        fullname: yup
            .string()
            .required('fullname field is required')
        ,
        email: yup
            .string()
            .email("This is not an Email")
            .required('Email field is required')
        ,
        username: yup
            .string()
            .required('username field is required')
            .min(3, 'your username is too short')
        ,
        password: yup
            .string()
            .required('password field is required')
            .min(3, 'your password is too short')

    });

    return schema;
}

export default SignupValidation;