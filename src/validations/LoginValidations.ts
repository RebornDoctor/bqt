import * as yup from 'yup';

function LoginValidation() {

    const schema = yup.object().shape({
        username: yup
            .string()
            .required('username field is required')
            .min(3, 'your username is too short')
        ,
        password: yup
            .string()
            .required('password field is required')
            .min(3, 'your password is too short')
    });

    return schema;
}

export default LoginValidation;