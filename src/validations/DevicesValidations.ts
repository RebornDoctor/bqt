import * as yup from 'yup';

function DevicesValidations(numberOfDevices: number) {
    const rules: any = {}
    for (let i = 0; i < numberOfDevices; i++) {
        rules[`devices-numbers-${i + 1}`] = yup.number().required('this field is required').min(1, 'you should add one device at least')

    }
    const schema = yup.object().shape(rules);

    return schema;
}

export default DevicesValidations;