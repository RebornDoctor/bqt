import * as yup from 'yup';

function PopupValidation() {

    const schema = yup.object().shape({
        numOfDevices: yup
            .number()
            .required('This field is required')
            .min(1, 'you must add 1 category at least')
            .max(4, 'you cant add more than four devices')
    });

    return schema;
}

export default PopupValidation;