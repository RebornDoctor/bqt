export interface Iuser {
    username: string
    password: string
    email: string
    fullname: string
}

export interface IpopupForm {
    numOfDevices: number
}

export interface Idevice {
    type: string
    num: number
}