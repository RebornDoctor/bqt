import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { SubmitHandler, useForm } from 'react-hook-form';
import { IpopupForm } from '../types';
import { yupResolver } from '@hookform/resolvers/yup';
import PopupValidationSchema from '../validations/PopupDialogValidation';

interface IpopupDialogProps {
    setnumOfDevices: React.Dispatch<React.SetStateAction<number>>
}

export default function PopupDialog(props: IpopupDialogProps) {
    const [open, setOpen] = React.useState(true);
    const { setnumOfDevices } = props
    const { register, handleSubmit, formState: { errors } } = useForm<IpopupForm>({ mode: "onTouched", resolver: yupResolver(PopupValidationSchema()) });

    const handleClose = () => {
        setOpen(false);
    };

    const onSubmit: SubmitHandler<IpopupForm> = ({ numOfDevices }) => {
        setnumOfDevices(numOfDevices)
        handleClose();
    }

    return (
        <div>
            <Dialog open={open}>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <DialogTitle>Network Graph</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            What is the number of devices categories you want to add?
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            style={{ marginTop: "10px" }}
                            id="devicesNum"
                            label="Number Of Devices Categories"
                            type="number"
                            fullWidth
                            variant="standard"
                            defaultValue={0}
                            {...register("numOfDevices")}
                        />
                        <small style={{ color: "red" }}>{errors.numOfDevices?.message}</small>
                    </DialogContent>
                    <DialogActions>
                        <Button type='submit'>Submit</Button>
                    </DialogActions>
                </form>
            </Dialog>
        </div>
    );
}