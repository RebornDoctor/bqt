import { Idevice, Iuser } from "./types";

export const fetchUser = (username: string): Iuser | null => {
    let user = localStorage.getItem(username);

    if (user)
        return JSON.parse(user);

    return null
}

export const registerUser = (user: Iuser): boolean => {
    const { username, password, fullname, email } = user;
    if (localStorage.getItem(username) !== null)
        return false;

    localStorage.setItem(username, JSON.stringify({ fullname, email, password }))
    return true
}


export const graphEdgesFactory = (currentDevices: Array<Idevice>) => {
    const edges = [];
    const nodes = [{ id: 0, label: "internet", title: "internet" }];
    let serv: Idevice | undefined;
    let rout: Idevice | undefined;
    let swi: Idevice | undefined;
    let others: Idevice | undefined
    let counter = 1;
    const availableEdges: any = { serv: [], rout: [], swi: [], others: [] };

    for (let i = 0; i < currentDevices.length; i++) {
        const device = currentDevices[i];
        switch (device.type) {
            case "Server":
                serv = device;
                break;
            case "Router":
                rout = device;
                break;
            case "Switch":
                swi = device;
                break;
            default:
                others = device
                break;
        }

    }

    if (serv) {
        for (let index = 0; index < serv.num; index++) {
            edges.push({ from: counter, to: 0 })
            nodes.push({ id: counter, label: `server${counter}`, title: "server" });
            availableEdges.serv.push(counter);
            counter++;
        }
    }

    if (rout) {
        for (let index = 0; index < rout.num; index++) {
            if (serv) {
                const dist = availableEdges.serv.pop();
                edges.push({ from: counter, to: dist })
                nodes.push({ id: counter, label: `router${counter}`, title: "router" });
                availableEdges.rout.push(counter);
                counter++;
                availableEdges.serv.unshift(dist);
            } else {
                edges.push({ from: counter, to: 0 })
                nodes.push({ id: counter, label: `router${counter}`, title: "router" });
                availableEdges.rout.push(counter);
                counter++;
            }
        }
    }

    if (swi) {
        for (let index = 0; index < swi.num; index++) {
            if (rout) {
                const dist = availableEdges.rout.pop();
                edges.push({ from: counter, to: dist })
                nodes.push({ id: counter, label: `switch${counter}`, title: "switch" });
                availableEdges.swi.push(counter);
                counter++;
                availableEdges.rout.unshift(dist);
            }
            else if (serv) {
                const dist = availableEdges.serv.pop();
                edges.push({ from: counter, to: dist })
                nodes.push({ id: counter, label: `switch${counter}`, title: "switch" });
                availableEdges.swi.push(counter);
                counter++;
                availableEdges.serv.unshift(dist);
            } else {
                edges.push({ from: counter, to: 0 })
                nodes.push({ id: counter, label: `switch${counter}`, title: "switch" });
                availableEdges.swi.push(counter);
                counter++;
            }
        }
    }

    if (others) {
        for (let index = 0; index < others.num; index++) {
            if (rout) {
                const dist = availableEdges.rout.pop();
                edges.push({ from: counter, to: dist })
                nodes.push({ id: counter, label: `laptop${counter}`, title: "laptop" });
                availableEdges.others.push(counter);
                counter++;
                availableEdges.rout.unshift(dist);
            }
            else if (serv) {
                const dist = availableEdges.serv.pop();
                edges.push({ from: counter, to: dist })
                nodes.push({ id: counter, label: `laptop${counter}`, title: "laptop" });
                availableEdges.others.push(counter);
                counter++;
                availableEdges.serv.unshift(dist);
            }
            else if (swi) {
                const dist = availableEdges.swi.pop();
                edges.push({ from: counter, to: dist })
                nodes.push({ id: counter, label: `laptop${counter}`, title: "laptop" });
                availableEdges.others.push(counter);
                counter++;
                availableEdges.swi.unshift(dist);
            }
            else {
                edges.push({ from: counter, to: 0 })
                nodes.push({ id: counter, label: `switch${counter}`, title: "switch" });
                availableEdges.others.push(counter);
                counter++;
            }
        }
    }

    return { edges: edges, nodes: nodes }
}